let logic_codes = {
    'L:P:0':{
        'description': 'Invalid Block',
        'iec_type': 'IB',
        'pse_data_type': 'LONG',
        'register_definition': 'Up to 1000 sequential registers',
        'scaled_required': false,
        'device_specific': 'generic_only',
        'notes': 'Defines invalid blocks of memory in the device. The driver does not include these registers in block reads'
    },
    'L:P:32':{
        'description': 'Scaled Register Signed',
        'iec_type': 'MV/CM',
        'pse_data_type': 'REAL',
        'register_definition': '1 or 2 registers',
        'scaled_required': false,
        'device_specific': 'generic',
        'notes': 'For a single register: treated as a signed value from –32,767 to +32,767. (- 32768 will result in a NA) For two registers: the registers will be concatenated together, the first register filling bits 16– 32 and the second register filling bits 0– 15. Values will range from –2,147,483,648 to – 2,147,483,647. Values can be scaled using a fixed scale or a scale register. NOTE: This logic code (and all REAL logic codes) has an accuracy of seven digits. Anything longer than seven digits should not be considered accurate.'
    },
}