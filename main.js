
    //Test
    let code = document.getElementById('code')
    code.value = 'T:MV;m:2024:u2;E:2;L:P:32'

    // Main function to decode the string
    function decode_string() {


        let code = document.getElementById('code').value
        let result = document.getElementById('result')
        let html = ''

        html += '<ul>'
        codes = code.split(';')
        codes.forEach((n, i) => {
            // let item_info = decode_item(n)
            // let item_html = generate_html(item_info)
            n = n.split(':')
            let att = n[0]
            let mod1 = n[1]
            let mod1_desc = ''
            let mod2 = ''

            if (function_codes['attributes'][att]['modifiers'][mod1] !== undefined) {
                mod1_desc = ' - ' + function_codes['attributes'][att]['modifiers'][mod1]
            }

            if (n.length == 3){
                switch(att){
                    case 'M':
                    case 'm':
                    case 'S':
                    case 's':
                    case 'C':
                    case 'c':
                    case 'I':
                    case 'i':
                        if(n[2].charAt(0) == 'u'){
                            mod2 = `<li>${n[2]}</li> <ul><li>Read a length of ${n[2].substring(1)} unsigned registers</li></ul>`
                        } else if(n[2].charAt(0) == 's'){   
                            mod2 = `<li>${n[2]}</li> <ul><li>Read a length of ${n[2].substring(1)} signed registers</li></ul>`
                        }
                        break
                    case 'L':
                        mod1 += `:${n[2]} - ${logic_codes[n[2]]['description']}`


                    default:
                        mod2 = ''
                }
                
            }
            
            // Add the comment as a tooltoip for each attribute
            item_html = `
                <li>${att} - ${function_codes['attributes'][att]['description']}</li>
                <ul>
                    <li>${mod1} ${mod1_desc}</li>
                    ${mod2}
                </ul>
            `


            html += item_html

        })
        html += '<li>Finished</li>'
        html += '</ul>'

        result.innerHTML = html
    }