// Create a json object of the function codes
function_codes = {
    'attributes':{
        'T':{
            'description':'(type) Required',
            'comment': 'Temporarily, this may return a string; when PowerSCADA Expert is upgraded to handle large integers, this will change',
            'modifiers':{
                'SS':'Single Status',
                'DS':'Double Status Enumeration',
                'ST':'String',
                'UT':'UTC Time',
                'MV':'Measured Value (float)',
                'CM':'Complex Measured Value (float)',
                'BC':'Binary Counter (integer)',
            }
        },
        'D':{
            'description':'Module -- Micrologic Devices',
            'comment': '',
            'modifiers':{
                'B':'BCM',
                'P':'PM',
                'M':'MM',
                'C':'CCM',
            }
        },
        // Register Types
        //TODO: Need to figire out the register number modifiers eg u2
        'M':{
            'description':'Start reading at holding registers in hexadecimal',
            'comment': 'Register Type',
            'modifiers':{}
        },
        'm':{
            'description':'Start reading at holding registers in decimal',
            'comment': 'Register Type',
            'modifiers':{}
        },
        'S':{
            'description':'Start reading at input coil (status register) in hexadecimal',
            'comment': 'Register Type',
            'modifiers':{}
        },
        's':{
            'description':'Start reading at input coil (status register) in decimal',
            'comment': 'Register Type',
            'modifiers':{}
        },
        'C':{
            'description':'Start writing at output coil (writable only) in hexadecimal',
            'comment': 'Register Type',
            'modifiers':{}
        },
        'c':{
            'description':'Start writing at output coil (writable only) in decimal',
            'comment': 'Register Type',
            'modifiers':{}
        },
        'I':{
            'description':'Start reading at input register (read only) in hexadecimal',
            'comment': 'Register Type',
            'modifiers':{}
        },
        'i':{
            'description':'Start reading at input register (read only) in decimal',
            'comment': 'Register Type',
            'modifiers':{}
        },
        // TODO: Need to figure out N (scale) and R (scale)
        // Priority
        'E':{
            'description':'Priority - single digit: 1, 2, or 3; default 2 is used if this is not included (1 = high, 2 = normal, 3 = low)',
            'comment': 'Defines the priority PowerSCADA Expert uses in processing data',
            'modifiers':{
                1: 'High',
                2: 'Normal (default)',
                3: 'Low'
            }
        },
        // Conversion Factor
        'V':{
            'description':'Use scientific notation without the decimal',
            'comment': 'Examples: 354E-3 = 0.354',
            'modifiers':{}
        },
        // Logice Code - Required
        // TODO: Creat json object for the logic codes
        'L':{
            'description':'The number that is used comes from the Logic Codes table.',
            'comment': 'L:P is the logic code for PowerLogic. Other codes may follow, such a L:I for ION.',
            'modifiers':{}
        },

    }
}